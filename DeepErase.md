# DeepErase

DeepErase: Weakly Supervised Ink Artifact Removal in Document Text Images

This might be a useful tool for our AIMS users that are using OCR to aggregate data for training.

Details are describe in [this paper](https://arxiv.org/pdf/1910.07070.pdf)


- link to BRC article on Tesseract
- Tool to facilitate creation of training dataset from text that is less that clear
- Used before OCR to improve the images for processing
- simple example using image of 'historic' text.
-
